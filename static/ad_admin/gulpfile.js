var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

var del = require('del');

// Stylus plugins
var nib = require('nib');
var rupture = require('rupture');

// Postcss
var lost = require('lost');
var autoprefixer = require('autoprefixer');

// Imagemin
var pngquant = require('imagemin-pngquant');
var jpegtran = require('imagemin-jpegtran');

// Scripts
var es = require('event-stream');
var glob = require('glob');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var spritesmith = require('gulp.spritesmith');
// Files paths
var paths = {
    images: {
        files: 'src/images/**/*.*',
        sprite: 'src/images/sprite/**/*.*'
    },
    stylus: {
        main: 'src/stylus/main.styl',
        files: 'src/stylus/**/*.styl',
        root: 'src/stylus'
    },
    scripts: {
       files: 'src/js/**/*.js',
       base: 'src/js',
       entries: 'src/js/**/*.entry.js'
   },
    dest: {
        styles: 'dist/css',
        scripts: 'dist/js',
        images: 'dist/images',
        sprite: 'dist/images/sprite'
    }
};

// Images dir clean
gulp.task('images-clean', function() {
    return del([
        paths.dest.images + '/**/*', `!${paths.dest.sprite}`, `!${paths.dest.sprite}/**/*`
    ]);
});

// Images
gulp.task('images', ['images-clean'], function() {
    return gulp.src([paths.images.files,  `!${paths.images.sprite}`])
        .pipe($.imagemin({
            progressive: true,
            svgoPlugins: [
                {
                    removeViewBox: false
                }
            ],
            use: [
                pngquant(),
                jpegtran()
            ]
        }))
        .pipe(gulp.dest(paths.dest.images));
});

 // Sprite
gulp.task('sprite', function() {
    var spriteData =
        gulp.src(paths.images.sprite) // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.styl',
                cssFormat: 'stylus',
                algorithm: 'binary-tree',
                cssTemplate: 'stylus.template.mustache',
                cssVarMap: function(sprite) {
                    sprite.name = 's-' + sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest(paths.dest.sprite)); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(paths.stylus.root)); // путь, куда сохраняем стили
});

// Styles dir clean
gulp.task('styles-clean', function() {
    return del([
        paths.dest.styles + '/**/*'
    ]);
});

// Stylus
gulp.task('stylus', ['styles-clean'], function() {

    var size = $.size({
        showFiles: true,
        gzip: true
    });

    return gulp.src(paths.stylus.main)
        .pipe($.plumber({
            errorHandler: $.notify.onError('Error: <%= error.message %>')
        }))
        .pipe($.stylint({
            config: '.stylintrc'
        }))
        .pipe($.stylint.reporter())
            .pipe($.stylus({
                url: 'data-uri',
                'include css': true,
                use: [
                    nib(),
                    rupture()
                ]
            }))
            .pipe($.postcss([
                lost(),
                autoprefixer({
                    browsers: ['last 2 versions'],
                    cascade: false
                }),
            ]))
        .pipe($.cleanCss({
            advanced: false
        }))
        .pipe($.rename({
            basename: 'styles',
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.dest.styles));
});

// Scripts dir clean
gulp.task('scripts-clean', function() {
    return del([
        paths.dest.scripts + '/**/*'
    ]);
});

// Scripts
gulp.task('scripts', ['scripts-clean'], function(done) {

    glob(paths.scripts.entries, function(err, entries) {
        if (err) done(err);

        var tasks = entries.map(function(entry) {

            var browserified = browserify({
                entries: entry,
                debug: true,
                transform: [

                    'babelify',
                    'uglifyify',

                    // See: package.json 'browserify-shim' section
                    'browserify-shim'
                ]
            });

            return browserified.bundle()
                .on('error', $.notify.onError('Error: <%= error.message %>'))
                .pipe(source(entry))
                .pipe($.rename(function(file) {

                    // Adding trailing slash to unify path cases
                    // it will help to trim basedir w/o additional trailing
                    // slash check
                    file.dirname += '/';

                    // removing base from file path
                    // to prevent getting sources directory in distribution folder
                    file.dirname = file.dirname.replace(
                        new RegExp(paths.scripts.base + '/', 'g'),
                        ''
                    );

                    // removing `.entry` suffix
                    file.basename = file.basename.replace(/.entry/, '');

                    // .min suffix adding
                    file.extname = '.min' + file.extname;

                    return file;
                }))
                .pipe(gulp.dest(paths.dest.scripts));
        });

        return es.merge(tasks).on('end', function() {
            $.livereload.reload();

            done();
        });

    });
});


// Watch
gulp.task('watch', function() {
    $.livereload.listen();
    gulp.watch(paths.images.sprite, ['sprite']);
    gulp.watch(paths.stylus.files, ['stylus']);
    gulp.watch(paths.scripts.files, ['scripts']);

    gulp.watch(paths.dest.styles + '/**/*.css').on('change', function(file) {
        $.livereload.changed(file.path);
    });
});

// Default
gulp.task('default', ['watch']);
