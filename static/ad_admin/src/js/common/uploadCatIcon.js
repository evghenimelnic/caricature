class UploadIconCat {
    constructor() {

        this.selectors = {
            uploadButton: '.js-upload-cat-icon',
            insertFilename: '.js-cat-icon',
            insertImage: '.js-current-icon-cat',
        };
        this.init();
    }

    init() {
        this.getNodes();
        this.events();
    }

    getNodes() {
        var _this = this;

        this.nodes = {

        };

        $.each(this.selectors, function(key, selector) {
            _this.nodes[key] = $(selector);
        });
    }



    events() {
        var self = this;
        self.nodes.uploadButton.change(function(e){
            e.preventDefault();
                var file_data = $(this).prop('files')[0];
                var form_data = new FormData();
                form_data.append('upload_file', file_data);
                form_data.append('path', 'category_icons');
                form_data.append('filename', $('#filename').val());
                $.ajax({
                    url: '/image_upload',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(resp){
                        console.log(resp);
                        self.nodes.insertFilename.val(resp.filename);
                        self.nodes.insertImage.attr("src", CONFIG.imageDomain + resp.upload_result);
                    }
                 });


        });

    }
}

export default UploadIconCat;
