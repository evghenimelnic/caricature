class Collapse {
    constructor() {

        this.selectors = {
            collapsePlus: '.js-collapsed',
        };
        this.init();
    }

    init() {
        this.getNodes();
        this.events();

    }

    getNodes() {
        var _this = this;

        this.nodes = {

        };

        $.each(this.selectors, function(key, selector) {
            _this.nodes[key] = $(selector);
        });

    }



    events() {
        var self = this;

        self.nodes.collapsePlus.click(function(e){
            e.preventDefault();

            var $this = $(e.currentTarget);
            console.log('clicked collapsed');



            var idscat = $this.attr('data-link');
            console.log(idscat);
            var urlBase = `/ajax?command=get_sub_categories&parent_id=`
            var url = urlBase + idscat;
            console.log(url);


            if ($('.collapse').hasClass('in')){
                console.log('close');
            } else {
                console.log('open');
                $.get(url, function(resp){
                     console.log(resp);


                });
            }

        })
    }
}

export default Collapse;