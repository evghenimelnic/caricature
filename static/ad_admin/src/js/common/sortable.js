class Sortable {
    constructor() {

        this.selectors = {
            sortabled: '#jsmaincatsort',
            sortableArrow:'.sortable-arrows'
        };
        this.init();
    }

    init() {
        this.getNodes();
        this.events();
        this.sortable();
    }

    getNodes() {
        var _this = this;

        this.nodes = {

        };

        $.each(this.selectors, function(key, selector) {
            _this.nodes[key] = $(selector);
        });

    }

    sortable() {
        var self = this;

         self.nodes.sortabled.sortable({
            handle: '.sortable-arrows',
        onEnd: function(){
                var rows = $('.js-main-cat');
                var ids = [];
                $.each(rows, function(i, item){
                    ids.push($(item).data('id'));
                });
                $.post('/ajax?command=save_category_order', JSON.stringify(ids), function(resp){
                    console.log(resp);
                });
            }

         }); // init
    }

    events() {

    }
}

export default Sortable;
