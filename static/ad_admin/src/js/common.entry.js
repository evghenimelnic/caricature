import UploadIconCat from './common/uploadCatIcon.js';
import Sortable from './common/sortable.js';
import Collapse from './common/collapse.js';
class Common {
    constructor() {
        this.selectors = {
        };
        this.init();
    }



    init() {
        this.getNodes();
        this.events();
        this.UploadIconCat = new UploadIconCat();
        this.Sortable = new Sortable();
        //this.Collapse = new Collapse();
    }
    getNodes() {
        var _this = this;
        this.nodes = {};
        $.each(this.selectors, function(key, selector) {
            _this.nodes[key] = $(selector);
        });
    }
    events(){


    }
}

new Common();
