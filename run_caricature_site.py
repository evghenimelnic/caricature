# -*- coding: utf-8 -*-
import logging
import settings

import os
import tornado.httpserver
import tornado.ioloop
import tornado.locale
import tornado.web
import controllers.base
import controllers.index
import lib.ui_methods
import lib.ui_modules

logger = logging.getLogger('tornado.general')


handlers = [
    # INDEX-------------------------------------------------------------------------------------------------------------
    tornado.web.url('/?', controllers.index.RedirectToLang, name='root'),
    tornado.web.url('/(?P<url_lang>en|fr)', controllers.index.Home, name='home'),
    tornado.web.url('/(?P<url_lang>en|fr)/', controllers.index.Home, name='home'),

    tornado.web.url('/(?P<url_lang>en|fr)/services', controllers.index.ServiceList, name='service.list'),
    tornado.web.url('/(?P<url_lang>en|fr)/service/(?P<slug>[a-zA-Z0-9-_/]+)', controllers.index.ServiceItem, name='service.item'),

    tornado.web.url('/(?P<url_lang>en|fr)/blog', controllers.index.Blog, name='blog'),
    tornado.web.url('/(?P<url_lang>en|fr)/blog/(?P<slug>[a-zA-Z0-9-_/]+)', controllers.index.BlogItem, name='blog.item'),

    tornado.web.url('/(?P<url_lang>en|fr)/contact', controllers.index.Contact, name='contact'),
    tornado.web.url('/(?P<url_lang>en|fr)/about', controllers.index.About, name='about'),

    tornado.web.url('/(?P<url_lang>en|fr)/gallery', controllers.index.GalleryPosts, name='gallery.posts'),
    tornado.web.url('/(?P<url_lang>en|fr)/gallery/(?P<slug>[a-zA-Z0-9-_/]+)', controllers.index.GalleryPost, name='gallery.post'),

    tornado.web.url('/ajax', controllers.index.Ajax, name='ajax')
]



tornado.locale.load_gettext_translations(os.path.join(os.path.dirname(__file__), 'locale'), 'messages')

application_settings = {
    'template_path': 'templates/',
    'static_path': 'static/caricature',
    'static_url_prefix': '/static/',
    'handlers': handlers,
    'xsrf_cookies': False,
    'debug': settings.debug,
    'login_url': '/ru/profile/login',
    'ui_modules':lib.ui_modules,
    'ui_methods': lib.ui_methods,
    'default_handler_class': controllers.base.NotFound,
    'static_handler_class': controllers.base.BaseHandlerStatic
}


application = tornado.web.Application(**application_settings)


if __name__ == "__main__":

    # If "xheaders" argument is True, tornado uses the "X-Real-Ip" and "X-Forwarded-For" headers
    # for setting self.request.remote_ip property in the request handlers
    http_server = tornado.httpserver.HTTPServer(application, xheaders=True)
    logger.info('Tornado version: %s' % tornado.version)

    if settings.debug:
        # run Tornado in one-process configuration
        http_server.listen(port=settings.ports['caricature_site'], address='0.0.0.0')
    else:
        # run Tornado in multi-processing configuration
        http_server.bind(port=settings.ports['caricature_site'], address='0.0.0.0')
        log_msg = 'Starting Tornado [{role}] with PID: {pid}'
        logger.info(log_msg.format(role='master', pid=os.getpid()))
        # Forks sub-processes (if num_processes == 0, then number of processes
        # will be equal to number of CPU cores)
        http_server.start(num_processes=16)
        logger.info(log_msg.format(role='worker', pid=os.getpid()))

    # configure Tornado loggers
    tornado.log.enable_pretty_logging()
    logging.getLogger('tornado.application').setLevel(logging.ERROR)
    logging.getLogger('tornado.access').setLevel(logging.WARNING)

    print('http://127.0.0.1:' + str(settings.ports['caricature_site']))
    tornado.ioloop.IOLoop.instance().start()
