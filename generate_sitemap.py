# -*- coding: utf-8 -*-
# !/usr/bin/env python
import time

import ast
import tornado.gen
import tornado.ioloop
import settings
import motor
import requests
import ujson
import models


@tornado.gen.coroutine
def run():

    xml = '<?xml version="1.0" encoding="UTF-8"?>' \
          '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
    xml += '''
        <url>
            <loc>https://becaricature.com/en</loc>
            <lastmod>2021-10-21</lastmod>
        </url>
        <url>
            <loc>https://becaricature.com/fr</loc>
            <lastmod>2021-10-21</lastmod>
        </url>        
        <url>
            <loc>https://becaricature.com/en/services</loc>
            <lastmod>2021-10-21</lastmod>
        </url>
        <url>
            <loc>https://becaricature.com/fr/services</loc>
            <lastmod>2021-10-21</lastmod>
        </url>            
        <url>
            <loc>https://becaricature.com/en/gallery</loc>
            <lastmod>2021-10-21</lastmod>
        </url>
        <url>
            <loc>https://becaricature.com/fr/gallery</loc>
            <lastmod>2021-10-21</lastmod>
        </url>             
        <url>
            <loc>https://becaricature.com/en/blog</loc>
            <lastmod>2021-10-21</lastmod>
        </url>
        <url>
            <loc>https://becaricature.com/fr/blog</loc>
            <lastmod>2021-10-21</lastmod>
        </url>    
        <url>
            <loc>https://becaricature.com/en/about</loc>
            <lastmod>2021-10-21</lastmod>
        </url>
        <url>
            <loc>https://becaricature.com/fr/about</loc>
            <lastmod>2021-10-21</lastmod>
        </url>
        <url>
            <loc>https://becaricature.com/en/contact</loc>
            <lastmod>2021-10-21</lastmod>
        </url>
        <url>
            <loc>https://becaricature.com/fr/contact</loc>
            <lastmod>2021-10-21</lastmod>
        </url>                                       
        '''

    services = yield models.mongo.services.find().to_list(length=None)
    for service in services:
        xml += '''
            <url>
                <loc>https://becaricature.com/en/service/{0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>
            <url>
                <loc>https://becaricature.com/fr/service/{0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>            
        '''.format(service['slug'])

    blog_categories = yield models.mongo.blog_categories.find().to_list(length=None)
    for blog_category in blog_categories:
        xml += '''
            <url>
                <loc>https://becaricature.com/en/blog?category={0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>
            <url>
                <loc>https://becaricature.com/fr/blog?category={0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>            
        '''.format(blog_category['slug'])

    blog_posts = yield models.mongo.blog_posts.find().to_list(length=None)
    for blog_post in blog_posts:
        xml += '''
            <url>
                <loc>https://becaricature.com/en/blog/{0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>
            <url>
                <loc>https://becaricature.com/fr/blog/{0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>            
        '''.format(blog_post['slug'])

    gallery_posts = yield models.mongo.gallery_posts.find().to_list(length=None)
    for gallery_post in gallery_posts:
        xml += '''
            <url>
                <loc>https://becaricature.com/en/gallery/{0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>
            <url>
                <loc>https://becaricature.com/fr/gallery/{0}</loc>
                <lastmod>2021-10-21</lastmod>
            </url>            
        '''.format(gallery_post['slug'])

    xml += '</urlset>'

    f = open("static/sitemap.xml", "w")
    f.write(xml)
    f.close()

if __name__ == '__main__':
    tornado.ioloop.IOLoop.instance().run_sync(run)


