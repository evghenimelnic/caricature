import urllib
import math
import tornado.web
import settings


def update_querystring(url, **kwargs):
    base_url = urllib.parse.urlsplit(url)
    query_args = urllib.parse.parse_qs(base_url.query)
    query_args.update(kwargs)
    for arg_name, arg_value in kwargs.items():
        if arg_value is None or arg_name in ['_pjax']:
            if arg_name in query_args or arg_name in ['_pjax']:
                del query_args[arg_name]

    query_string = urllib.parse.urlencode(query_args, True)
    return urllib.parse.urlunsplit((base_url.scheme, base_url.netloc, base_url.path, query_string, base_url.fragment))


class Paginator(tornado.web.UIModule):

    def render_string(self, path, **kwargs):
        return super(Paginator, self).render_string(settings.CURRENT_PROJECT + '/' + path, **kwargs)

    """Pagination links display."""

    def render(self, page, page_size, results_count, prefix=''):
        pages = int(math.ceil(results_count / page_size)) if results_count else 0

        def get_page_url(page):
            # don't allow ?page=1
            if page <= 1:
                page = None
            return update_querystring(self.request.uri, page=page, _pjax='')

        next = page + 1 if page < pages else None
        previous = page - 1 if page > 1 else None

        return self.render_string('ui_templates/pagination.html', page=page, pages=pages, next=next, previous=previous,
                                  get_page_url=get_page_url, results_count=results_count, page_size=page_size)
