import unidecode
import dateutil
import datetime
import re


def slugify(text):
    text = unidecode.unidecode(text).lower()
    return re.sub(r'\W+', '-', text)


def get_now():
    return datetime.datetime.now(tz=dateutil.tz.gettz('Europe/Chisinau'))
