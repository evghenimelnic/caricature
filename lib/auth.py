from __future__ import absolute_import, division, print_function
import sys
import base64
import binascii
import functools
import hashlib
import hmac
import time
import datetime
import controllers.base

from tornado import gen
from tornado import httpclient
from tornado import escape
import tornado.auth
import tornado.httputil
import logging
import json
import traceback
import urllib.parse as urlparse
import urllib.parse as urllib_parse
import settings
import tornado.web

long = int


def computeMD5hash(my_string):
    m = hashlib.md5()
    m.update(my_string.encode('utf-8'))
    return m.hexdigest()


class OAuthHandler(controllers.base.BaseHandler):
    @staticmethod
    def convert_birthday(provider_data, date_format):
        try:
            return int(time.mktime(datetime.datetime.strptime(provider_data, date_format).timetuple())) if (
                provider_data) else None
        except:
            return None

    def handle_oauth_failure(self, exc_info):
        """User-friendly error reporting

        :param exc_info: exception context
        """
        etype, ex, tb = exc_info
        logging.error('%s %s', str(self.request), traceback.format_exception(etype, ex, tb))

        # return to beginning
        self.render('special/social_error.html', **{
            'error_code': getattr(ex, 'error_code', -1)
        })

    def provider(self):
        raise NotImplementedError()

    def authorize_params(self):
        raise NotImplementedError()

    def authenticate_params(self):
        raise NotImplementedError()

    def external_user_id(self, user_info):
        raise NotImplementedError()

    def external_profile(self, user_info):
        raise NotImplementedError()

    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        if 'code' not in self.request.arguments:
            # authorize
            yield self.authorize_redirect(**self.authorize_params())
            raise tornado.web.HTTPError(302)

        try:
            # authenticate
            user_info = yield self.get_authenticated_user(**self.authenticate_params())


            # if not self.external_profile(user_info).get('email'):
            #     if 'facebook' in self.request.uri:
            #         logging.error('%s %s %s', str(self.request), json.dumps(user_info), json.dumps(self.authenticate_params()))
            #
            #     self.render('special/social_no_email.html')
            #
            #     raise tornado.web.Finish(json.dumps({'msg': 'no email'}))

        except Exception as ex:
            print(ex)
            self.handle_oauth_failure(sys.exc_info())
        else:
            referer = self.request.headers.get('Referer')

            if self.provider() == 'ok':
                user = yield self.login_ok_user(user_info)
            elif self.provider() == 'ya':
                user = yield self.login_ya_user(user_info)


            self.render('special/social_login.html', **{
                'user_token': user['_id']
            })


class VkOAuth2Mixin(tornado.auth.OAuth2Mixin):
    """Google authentication using OAuth2.

    In order to use, register your application with Google and copy the
    relevant parameters to your application settings.

    * Go to the Google Dev Console at http://console.developers.google.com
    * Select a project, or create a new one.
    * In the sidebar on the left, select APIs & Auth.
    * In the list of APIs, find the Google+ API service and set it to ON.
    * In the sidebar on the left, select Credentials.
    * In the OAuth section of the page, select Create New Client ID.
    * Set the Redirect URI to point to your auth handler
    * Copy the "Client secret" and "Client ID" to the application settings as
      {"google_oauth": {"key": CLIENT_ID, "secret": CLIENT_SECRET}}

    .. versionadded:: 3.2
    """
    _OAUTH_AUTHORIZE_URL = "https://oauth.vk.com/authorize"
    _OAUTH_ACCESS_TOKEN_URL = "https://oauth.vk.com/access_token"
    _OAUTH_NO_CALLBACKS = False


    @tornado.auth._auth_return_future
    def get_authenticated_user(self, redirect_uri, code, callback):
        """Handles the login for the Google user, returning an access token.

        The result is a dictionary containing an ``access_token`` field
        ([among others](https://developers.google.com/identity/protocols/OAuth2WebServer#handlingtheresponse)).
        Unlike other ``get_authenticated_user`` methods in this package,
        this method does not return any additional information about the user.
        The returned access token can be used with `OAuth2Mixin.oauth2_request`
        to request additional information (perhaps from
        ``https://www.googleapis.com/oauth2/v2/userinfo``)

        Example usage:

        .. testcode::

            class GoogleOAuth2LoginHandler(tornado.web.RequestHandler,
                                           tornado.auth.GoogleOAuth2Mixin):
                @tornado.gen.coroutine
                def get(self):
                    if self.get_argument('code', False):
                        access = yield self.get_authenticated_user(
                            redirect_uri='http://your.site.com/auth/google',
                            code=self.get_argument('code'))
                        user = yield self.oauth2_request(
                            "https://www.googleapis.com/oauth2/v1/userinfo",
                            access_token=access["access_token"])
                        # Save the user and access token with
                        # e.g. set_secure_cookie.
                    else:
                        yield self.authorize_redirect(
                            redirect_uri='http://your.site.com/auth/google',
                            client_id=self.settings['google_oauth']['key'],
                            scope=['profile', 'email'],
                            response_type='code',
                            extra_params={'approval_prompt': 'auto'})

        .. testoutput::
           :hide:

        """
        http = self.get_auth_http_client()
        body = urllib_parse.urlencode({
            "redirect_uri": redirect_uri,
            "code": code,
            "client_id": settings.vk['api_key'],
            "client_secret": settings.vk['secret'],
            "grant_type": "authorization_code",
        })

        http.fetch(self._OAUTH_ACCESS_TOKEN_URL,
                   functools.partial(self._on_access_token, callback),
                   method="POST", headers={'Content-Type': 'application/x-www-form-urlencoded'}, body=body)

    def _on_access_token(self, future, response):
        """Callback function for the exchange to the access token."""
        if response.error:
            future.set_exception(tornado.auth.AuthError('VK auth error: %s' % str(response)))
            return

        args = escape.json_decode(response.body)
        future.set_result(args)


class YandexOAuth2Mixin(tornado.auth.OAuth2Mixin):
    """ Yandex implementation

    It's nothing special but the same two methods:
        authorize redirect
        get authenticated user
    """
    _OAUTH_AUTHORIZE_URL = 'https://oauth.yandex.ru/authorize'
    _OAUTH_ACCESS_TOKEN_URL = 'https://oauth.yandex.ru/token'
    _OAUTH_USER_INFO_URL = 'https://login.yandex.ru/info?format=json&oauth_token=%s'

    @staticmethod
    def _get_auth_http_client():
        return httpclient.AsyncHTTPClient()

    def _oauth_request_token_url(self, redirect_uri=None, client_id=None, client_secret=None, code=None,
                                 extra_params=None):
        url = self._OAUTH_ACCESS_TOKEN_URL
        args = dict(
            code=code,
            client_id=client_id,
            client_secret=client_secret,
        )
        if extra_params:
            args.update(extra_params)
        return url, urllib_parse.urlencode(args)

    @gen.coroutine
    def get_authenticated_user(self, client_id=None, client_secret=None, code=None, extra_params=None):
        http = self._get_auth_http_client()

        # request access token
        token_request_url, token_request_args = self._oauth_request_token_url(
            client_id=client_id, client_secret=client_secret, code=code,
            extra_params=extra_params)
        token_response = yield http.fetch(token_request_url,
                                          method="POST",
                                          headers={'Content-Type': 'application/x-www-form-urlencoded'},
                                          body=token_request_args)
        if token_response.error:
            raise tornado.auth.AuthError(error_code=token_response.error)

        access_token = escape.json_decode(token_response.body)

        # request user info
        api_response = yield http.fetch(self._OAUTH_USER_INFO_URL % access_token['access_token'])
        if api_response.error:
            raise tornado.auth.AuthError(error_code=api_response.error)

        user_info = escape.json_decode(api_response.body)

        raise tornado.gen.Return(user_info)


class OKMixin(tornado.auth.OAuth2Mixin):
    _OAUTH_ACCESS_TOKEN_URL = "http://api.ok.ru/oauth/token.do"
    _OAUTH_AUTHORIZE_URL = "https://connect.ok.ru/oauth/authorize"
    _OAUTH_CURRENT_USER_URL = "https://api.ok.ru/fb.do"

    @staticmethod
    def _get_auth_http_client():

        return httpclient.AsyncHTTPClient()

    @gen.coroutine
    def get_authenticated_user(self, redirect_uri, code, client_id=None, client_secret=None, callback=None):

        http_client = tornado.httpclient.AsyncHTTPClient()

        body = urllib_parse.urlencode({
            "redirect_uri": redirect_uri,
            "code": code,
            "client_id": client_id,
            "client_secret": client_secret,
            "grant_type": "authorization_code",
        })

        token_response = yield http_client.fetch(
            self._OAUTH_ACCESS_TOKEN_URL,
            method="POST",
            headers={'Content-Type': 'application/x-www-form-urlencoded'},
            body=body)

        if token_response.error:
            logging.error('OK auth error %s', token_response)

            raise tornado.auth.AuthError(error_code=token_response.error)

        access_token = escape.json_decode(token_response.body)
        access_token = access_token['access_token']

        secret_key = computeMD5hash(access_token + settings.ok['application_secret_key'])

        api_response_body = {
            "application_key": settings.ok['application_key'],
            "format": "json",
            "method": "users.getCurrentUser",
            "fields": "email,first_name,last_name,birthday,gender",
            "scope": settings.ok['scope']
        }

        api_responce_keys = list(api_response_body.keys())
        api_responce_keys.sort()
        signature_row = ''
        for k in api_responce_keys:
            signature_row += ("%s=%s" % (k, api_response_body[k]))

        signature_row += secret_key
        sig = computeMD5hash(signature_row)

        api_response_body['sig'] = sig
        api_response_body["access_token"] = access_token
        _OAUTH_CURRENT_USER_URL = "https://api.ok.ru/fb.do"
        api_response = yield http_client.fetch(_OAUTH_CURRENT_USER_URL, method="POST",
                                        body=urllib_parse.urlencode(api_response_body))

        if api_response.error:
            logging.error('OK auth error %s', api_response)
            raise Exception(error_code=api_response.error)

        api_response_info = escape.json_decode(api_response.body)

        user_info = {
            'external_user_id': api_response_info.get('uid'),
            'email': api_response_info.get('email'),
            'first_name': api_response_info.get('first_name'),
            'last_name': api_response_info.get('last_name'),
            #'birthdate': self.convert_birthday(api_response_info.get('birthday'), '%Y-%m-%d'),
            'sex': api_response_info.get('gender', 'undefined')[:1]
        }
        raise tornado.gen.Return(user_info)


class MailRuOAuth2Mixin(tornado.auth.OAuth2Mixin):
    """ Mail.ru implementation

    As usual we have two methods here:
        authorize redirect
        get authenticated user
    """
    _OAUTH_ACCESS_TOKEN_URL = 'https://connect.mail.ru/oauth/token'
    _OAUTH_AUTHORIZE_URL = 'https://connect.mail.ru/oauth/authorize'
    _OAUTH_API_URL = 'http://www.appsmail.ru/platform/api'

    @staticmethod
    def _get_auth_http_client():
        return httpclient.AsyncHTTPClient()

    def _oauth_request_token_url(self, redirect_uri=None, client_id=None, client_secret=None, code=None,
                                 extra_params=None):
        url = self._OAUTH_ACCESS_TOKEN_URL
        args = dict(
            redirect_uri=redirect_uri,
            code=code,
            client_id=client_id,
            client_secret=client_secret,
        )
        if extra_params:
            args.update(extra_params)
        return url, urllib_parse.urlencode(args)

    @gen.coroutine
    def get_authenticated_user(self, redirect_uri=None, client_id=None, client_secret=None, code=None,
                               extra_params=None):
        http = self._get_auth_http_client()

        # request access token
        token_request_url, token_request_args = self._oauth_request_token_url(
            redirect_uri=redirect_uri,
            client_id=client_id,
            client_secret=client_secret, code=code,
            extra_params=extra_params)

        token_response = yield http.fetch(
            token_request_url,
            method='POST',
            headers={'Content-Type': 'application/x-www-form-urlencoded'},
            body=token_request_args)
        if token_response.error:
            logging.error('Mail.ru auth error %s', str(token_response))
            raise tornado.auth.AuthError(error_code=token_response.error)

        access_token = escape.json_decode(token_response.body)

        # request user info
        sig_params = 'app_id=%smethod=users.getInfosecure=1session_key=%s%s' % (
            client_id, access_token['access_token'], client_secret)
        api_params = {
            'method': 'users.getInfo',
            'secure': 1,
            'app_id': client_id,
            'session_key': access_token['access_token'],
            'sig': hashlib.md5(sig_params).hexdigest()
        }
        api_url = tornado.httputil.url_concat(self._OAUTH_API_URL, api_params)
        api_response = yield http.fetch(api_url)
        if api_response.error:
            logging.error('Mail.ru auth error %s', str(api_response))
            raise tornado.auth.AuthError(error_code=api_response.error)

        user_info = escape.json_decode(api_response.body)

        try:
            if user_info[0].get('pic_190') and user_info[0].get("has_pic"):
                user_info[0]["avatar_url"] = user_info[0]["pic_190"]
        except Exception as ex:
            logging.exception(str(ex))

        raise tornado.gen.Return(user_info[0])