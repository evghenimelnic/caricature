# coding=utf-8
from datetime import datetime as dt, date
import lib.utils


import settings
import pytz
import arrow
import orjson
import urllib
import datetime
import constants
import dateutil.relativedelta


def update_querystring(handler, url, skipped=[], **kwargs):
    base_url = urllib.parse.urlsplit(url)
    query_args = urllib.parse.parse_qs(base_url.query)
    query_args.update(kwargs)
    kwargs.update(query_args)
    skipped = skipped + ['_pjax']
    for arg_name, arg_value in kwargs.items():
        if arg_value is None or arg_name in skipped:
            if arg_name in query_args or arg_name in skipped:
                del query_args[arg_name]

    query_string = urllib.parse.urlencode(query_args, True)
    return urllib.parse.urlunsplit((base_url.scheme, base_url.netloc, base_url.path, query_string, base_url.fragment))


def print_cat_tree_option(handler, cat, sub_options_count=0, selected_cat_id=None):
    sub_options = ''
    for child in cat['children']:
        sub_options += print_cat_tree_option(handler, child, sub_options_count + 1, selected_cat_id=selected_cat_id)

    selected = ' selected' if cat['_id'] == selected_cat_id else ''

    if cat['type'] == 'final_with_features':
        return '<option value="%s" disabled %s>%s</option>%s' % (cat['_id'], selected, '---' * sub_options_count + cat['title']['en'], sub_options)
    else:
        return '<option value="%s" %s>%s</option>%s' % (cat['_id'], selected, '---' * sub_options_count + cat['title']['en'], sub_options)


def print_job_cat_tree_option(handler, cat, sub_options_count=0, selected_cat_id=None):
    sub_options = ''
    for child in cat['children']:
        sub_options += print_job_cat_tree_option(handler, child, sub_options_count + 1, selected_cat_id=selected_cat_id)

    selected = ' selected' if cat['_id'] == selected_cat_id else ''

    if cat['is_final']:
        return '<option value="%s" disabled %s>%s</option>%s' % (cat['_id'], selected, '---' * sub_options_count + cat['title']['en'], sub_options)
    else:
        return '<option value="%s" %s>%s</option>%s' % (cat['_id'], selected, '---' * sub_options_count + cat['title']['en'], sub_options)


def print_post_job_cat_tree_option(handler, cat, sub_options_count=0, selected_cat_id=None):
    sub_options = ''
    for child in cat['children']:
        sub_options += print_post_job_cat_tree_option(handler, child, sub_options_count + 1, selected_cat_id=selected_cat_id)

    selected = ' selected' if cat['_id'] == selected_cat_id else ''

    if not cat['is_final']:
        return '<option value="%s" disabled %s>%s</option>%s' % (cat['_id'], selected, '---' * sub_options_count + cat['title']['en'], sub_options)
    else:
        return '<option value="%s" %s>%s</option>%s' % (cat['_id'], selected, '---' * sub_options_count + cat['title']['en'], sub_options)


def print_post_job_cat_tree_option_for_search(handler, cat, sub_options_count=0, selected_cat_id=None):
    sub_options = ''
    for child in cat['children']:
        sub_options += print_post_job_cat_tree_option_for_search(handler, child, sub_options_count + 1, selected_cat_id=selected_cat_id)

    selected = ' selected' if cat['_id'] == selected_cat_id else ''

    return '<option value="%s" %s>%s</option>%s' % (cat['slug'], selected, '---' * sub_options_count + cat['title']['en'], sub_options)


def in_url(handler, part_url):
    return part_url in handler.request.path


def image(handler, path, filename):
    if path.startswith('/'):
        path = path[1:]
    if path.endswith('/'):
        path = path[:-1]

    if settings.debug:
        return 'http://127.0.0.1:9999/image_data/caricature/%s/%s/%s' % (path, filename[:2], filename)
    else:
        return 'https://becaricature.com/image_data/caricature/%s/%s/%s' % (path, filename[:2], filename)


def image_admin(handler, path, filename):
    if path.startswith('/'):
        path = path[1:]
    if path.endswith('/'):
        path = path[:-1]

    if settings.debug:
        return 'http://127.0.0.1:2222/static/image_data/%s/%s/%s' % (path, filename[:2], filename)
    else:
        return '//forall.ie/static/image_data/%s/%s/%s' % (path, filename[:2], filename)



def time_ago(handler, d, now=None):
    return timeago.format(d, now, handler.locale.code)


def format_date(handler, d, fmt='D MMM YYYY, H:mm', use_today=False, locale=''):
    """Format UTC date (represented as UNIX timestamp or datetime object) according to specified format.

    Args:
        self (object): Request handler instance.
        d (string, int, float, datetime.datetime, datetime.date): UTC date (as UNIX timestamp or datetime object)
        fmt: str
        use_today: bool
        locale: locale

    Returns:
        str - formatted date
    """
    if not d:
        if use_today:
            d = dt.now()
        else:
            return ''
    if isinstance(d, str):
        d = arrow.get(d).datetime
    elif isinstance(d, (int, float)):
        d = dt.utcfromtimestamp(d).replace(tzinfo=pytz.UTC)
    elif not isinstance(d, date) and isinstance(d, dt.date):
        # double check, because datetime.datetime is instance of datetime.date
        d = dt.datetime.combine(d, dt.time(0, 0)).replace(tzinfo=pytz.UTC)

    return arrow.get(d).to('Europe/Chisinau').format(fmt, locale=locale or handler.locale.code)


def iter_months(handler, selected_month=None, fmt='MMMM', locale=''):

    res = []
    now = handler.now
    for m in range(1, 13):
        res.append({'no': m, 'name': arrow.get(now.replace(month=m)).to('Europe/Chisinau').format(fmt, locale=locale or handler.locale.code)})

    return res


def chunks(handler, l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


def show_preview_img(handler, ad):
    if ad['images']:
        filename = ad['images'][0]
        return 'https://tot.md/image_data/tot/preview/%s/%s' % (filename[:2], filename)
    else:
        return handler.static_url('images/no-image.svg')


def show_el_preview_img(handler, ad):
    if '_source' in ad:
        ad = ad['_source']
    if ad['images']:
        filename = ad['images'][0]
        return 'https://tot.md/image_data/tot/preview/%s/%s' % (filename[:2], filename)
    else:
        return handler.static_url('images/no-image.svg')


def ad_image_src(handler, filename, size='800x600'):
    if size == 'company_logo' and not filename:
        return handler.static_url('images/no-image.svg')

    return 'https://tot.md/image_data/tot/%s/%s/%s' % (size, filename[:2], filename)


def make_http_url(handler, url):
    return url if url.startswith('http') else 'http://' + url


def format_timedelta(handler, date, threshold=0.9, add_direction=True):
    """Format timedelta between now and specified date (represented as UNIX timestamp or datetime object).

    Args:
        handler (object): Request handler instance.
        date (int, float, datetime): UTC date (as UNIX timestamp or datetime object)

    Returns:
        str - formatted timedelta

    """
    if isinstance(date, (int, float)):
        date = datetime.datetime.utcfromtimestamp(date).replace(tzinfo=pytz.UTC)

    delta = date - datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
    timedelta_str = babel.dates.format_timedelta(
        delta=delta,
        threshold=threshold,
        add_direction=add_direction,
        locale=handler.locale.code)
    return timedelta_str.lower()


def absolute_reverse_url(handler, name, *args):
    """Returns a full URL for handler name.

    Args:
        name (str): name of request handler.

    """
    base = handler.request.protocol + "://" + handler.request.host
    return base + handler.reverse_url(name, *args)


def number_thousands(handler, number, round_ndigits=None, format_template='{0:,.2f}'):
    """Format integer numbers using the space as a thousands separator.

    Args:
        number (int): integer number.
        round_ndigits (int): integer round value.

    Returns:
        str - formatted number as string.
    """

    if not isinstance(number, (int, float)):
        return number

    if round_ndigits:
        number = int(round(number, round_ndigits))

    if isinstance(number, float):
        return format_template.format(number).replace(',', ' ').replace('.', ',')
    elif isinstance(number, int):
        return '{0:,d}'.format(int(number)).replace(',', ' ')
    else:
        return number


def date_shift(handler, days_to_shift):
    today = datetime.date.today()
    shifted_date = today + dateutil.relativedelta.relativedelta(days=days_to_shift)

    return shifted_date


def format_ru_syntax_age(handler, age):

    if age > 20 and age%10 == 1:
        return 'year.single'
    elif age > 20 and age%10 == 2 and age%10 == 3 and age%10 == 4:
        return 'year.single.incline'
    else:
        return 'year.plural'


def highlight_code(handler, data):
    formatter = HtmlFormatter(
        cssstyles='font-family: monospace; padding: 20px 30px;',
        noclasses=True,
        style='friendly')

    try:
        json_str = ujson.dumps(data, indent=4, ensure_ascii=False).encode('utf-8')
        lexer = JsonLexer()
    except TypeError:
        json_str = str(data)
        lexer = PythonLexer()

    html = highlight(json_str, lexer, formatter)

    return html


def class_name(handler, class_name, toggle):
    return class_name if toggle else ''


def selected(handler, toggle):
    return 'selected' if toggle else ''


def checked(handler, toggle):
    return 'checked' if toggle else ''


def disabled(handler, toggle):
    return 'disabled' if toggle else ''


def readonly(handler, toggle):
    return 'readonly' if toggle else ''


def required(handler, toggle):
    return 'required' if toggle else ''


def is_today(handler, date):
    today = False

    if isinstance(date, datetime.date):
        today = date == datetime.date.today()

    return today


def share_url(handler, social_type, page_url, page_title):
    """Format share url for popular social networking services.

    Args:
        social_type (string): social network service name
        page_url (string): url of sharing page
        page_title (string): title of sharing page

    Return:
        str - url for social sharing
    """
    SOCIAL_NETWORKS_URL_TEMPLATES = {
        'fb': constants.FACEBOOK_SHARE_URL,
        'twitter': constants.TWITTER_SHARE_URL,
        'linkedin': constants.LINKEDIN_SHARE_URL,
        'ok': constants.ODNOKLASSNIKI_SHARE_URL,
        'vk': constants.VKONTAKTE_SHARE_URL,
    }

    url = SOCIAL_NETWORKS_URL_TEMPLATES[social_type].format(
        page_url=page_url,
        page_title=page_title
    )

    return url


def canonical_url(handler):

    protocol = handler.request.protocol
    host = handler.request.host
    uri = handler.request.uri

    # Strip third level mobile domain: "m."
    if host.startswith('m.'):
        host = host[2:]

    url = '{protocol}://{host}{uri}'.format(
        protocol=protocol,
        host=host,
        uri=uri
    )

    return url


def text_limit(handler, text, limit=256):
    return text[0:limit]


def get_video_embed_url(handler, video_url):
    parsed_url = urlparse.urlparse(video_url)

    if parsed_url.netloc in {'www.youtube.com', 'youtu.be'}:
        video_id = urlparse.parse_qs(parsed_url.query)['v'][0]
        url = '//www.youtube.com/embed/%s' % video_id
    elif parsed_url.netloc == 'play.md':
        video_id = parsed_url.path.split('/')[-1]
        url = '//play.md/embed/%s?title=false' % video_id
    elif parsed_url.netloc == 'vimeo.com':
        video_id = parsed_url.path.split('/')[-1]
        url = '//player.vimeo.com/video/%s' % video_id

    return url