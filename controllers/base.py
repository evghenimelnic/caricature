import uuid
import datetime
import tornado.web
import tornado.gen
import settings
import constants
import models



class NotFoundHandler(tornado.web.RequestHandler):
    def prepare(self):  # for all methods
        raise tornado.web.HTTPError(
            status_code=404,
            reason="Invalid resource path."
        )


class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

        self.lang = ''
        self.sub_module = 'ads'
        self.breadcrumbs = []
        self.seo = {'page_title': '', 'description': '', 'keywords': ''}

    def seo_page_title(self, title):
        self.seo['page_title'] = title

    def seo_description(self, description):
        self.seo['description'] = description

    def get_current_user(self):

        cookie_user_id = self.get_cookie('user_id')
        if cookie_user_id:
            return cookie_user_id
        return None

    def render(self, template_name, **kwargs):
        kwargs['lang'] = self.lang
        kwargs['seo'] = self.seo
        kwargs['breadcrumbs'] = self.breadcrumbs
        kwargs['sub_module'] = self.sub_module

        kwargs['root_service_list'] = self.service_list
        kwargs['root_blog_categories'] = self.blog_categories

        super(BaseHandler, self).render(template_name, **kwargs)

    def _(self, message, plural_message=None, count=None):
        return self.locale.translate(message, plural_message, count)

    @property
    def remote_ip(self):
        return self.request.headers.get('X-Forwarded-For', self.request.headers.get('X-Real-Ip', self.request.remote_ip))

    @tornado.gen.coroutine
    def prepare(self):

        # NOTE: pop "url_lang" from self.path_kwargs dict to omit indication in each method of each handler
        # don't validate url_lang, because it's always correct (see URL regex)
        cookie_lang = self.get_cookie('me_current_lang', '').strip()
        url_lang = self.path_kwargs.pop('url_lang', '').strip()
        if url_lang and url_lang in constants.LANGUAGES:
            self.lang = url_lang
        elif cookie_lang and cookie_lang in constants.LANGUAGES:
            self.lang = cookie_lang
        else:
            for acc_lang in self.request.headers.get('Accept-Language').split(','):
                for lang in constants.LANGUAGES:
                    if lang in acc_lang:
                        self.lang = lang
                        break
                if self.lang:
                    break
            if not self.lang:
                self.lang = 'en'
        self.set_cookie('caricature_current_lang', self.lang)

        self.service_list = yield models.mongo.services.find().sort([('order', 1)]).to_list(length=None)
        self.blog_categories = yield models.mongo.blog_categories.find().sort([('order', 1)]).to_list(length=None)

    def get_int(self, name):
        return int(self.get_argument(name))

    def get_int_or_empty(self, name):
        return int(self.get_argument(name)) if self.get_argument(name, '').strip().isnumeric() else ''

    def get_user_locale(self):
        """Override to determine the locale from the authenticated user.

        If None is returned, we fall back to get_browser_locale(). This method should return
        a tornado.locale.Locale object, most likely obtained via a call like tornado.locale.get("en")

        http://www.tornadoweb.org/en/stable/web.html#tornado.web.RequestHandler.get_user_locale
        """
        return tornado.locale.get(self.lang)

    @property
    def is_ajax(self):
        return self.request.headers.get("X-Requested-With") == "XMLHttpRequest"

    @property
    def is_pjax(self):
        return self.request.headers.get("X-PJAX") == "true"

    def get_page(self, limit):
        page = int(self.get_argument('page', 1))
        start = limit * (page - 1)
        return page, start, limit


class NotFound(BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        self.render('pages/404.html')


class BaseHandlerStatic(tornado.web.StaticFileHandler):
    @classmethod
    def make_static_url(cls, _settings, path, include_version=True):
        return super().make_static_url(_settings, path, include_version)
