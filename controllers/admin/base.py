import datetime

import tornado.web
import tornado.gen
import models
import settings
import lib.utils


class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

        self.page_title = ''
        self.breadcrumbs = []
        self.lang = 'ru'
        self.now = datetime.datetime.now()

    def set_page_title(self, title):
        self.page_title = title

    @tornado.gen.coroutine
    def prepare(self):
        """Called at the beginning of a request before get/post/etc.

        Override this method to perform common initialization regardless of the request method.

        """
        cookie_lang = self.get_cookie('lang', '')[:2]     # get only 2 first characters

        # NOTE: pop "url_lang" from self.path_kwargs dict to omit indication in each method of each handler
        # don't validate url_lang, because it's always correct (see URL regex)
        url_lang = self.path_kwargs.pop('lang', 'ru')



        self.lang = url_lang

    def render(self, template_name, **kwargs):
        kwargs['lang'] = self.lang
        kwargs['page_title'] = self.page_title
        kwargs['breadcrumbs'] = self.breadcrumbs

        super(BaseHandler, self).render(template_name, **kwargs)


    def get_page(self, limit):
        page = int(self.get_argument('page', 1))
        start = limit * (page - 1)
        return page, start, limit

    def get_int(self, arg_name):
        return int(self.get_argument(arg_name))

    @property
    def remote_ip(self):
        return self.request.headers.get('X-Forwarded-For', self.request.headers.get('X-Real-Ip', self.request.remote_ip))

