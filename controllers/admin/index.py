# -*- coding: utf-8 -*-
import hashlib
import tornado.web
import tornado.gen
import controllers.admin.base
import uuid
import time
import datetime
import settings
import os
import mimetypes
import lib.utils
import tornado.httpclient
import pymongo
import settings
import models
import models.objects


class Home(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):

        self.set_page_title('Admin Dashboard')
        self.render('admin/pages/index.html')



class ServiceList(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        action = self.get_argument('action', '')

        if action == 'delete':
            yield models.mongo.services.remove({'_id': self.get_argument('service_id')})
            self.redirect(self.reverse_url('service.list'))
        else:

            service_list = yield models.mongo.services.find().sort([('order', 1)]).to_list(length=None)
            self.render('admin/pages/manage/service_list.html', service_list=service_list)


class ServiceItem(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        service_id = self.get_argument('service_id', '')

        if service_id:
            service_item = yield models.mongo.services.find_one({'_id': service_id})
        else:
            service_item = models.objects.ServiceItem
            service_item['posted_at'] = self.now

        gallery_posts = yield models.mongo.gallery_posts.find().to_list(length=None)

        self.render('admin/pages/manage/service_item.html', service_item=service_item, gallery_posts=gallery_posts)

    @tornado.gen.coroutine
    def post(self):

        service_id = self.get_argument('service_id', '')

        doc = {'slug': self.get_argument('slug'),
               'posted_at': datetime.datetime.strptime(self.get_argument('posted_at'), '%H:%M %d.%m.%Y'),
               'image_filename': self.get_argument('image_filename'),
               'order': int(self.get_argument('order')),
               'name': {},
               'short_desc': {},
               'show_on_main_page': bool(int(self.get_argument('show_on_main_page', 0))),
               'body': {},
               'gallery_post_ids': self.get_arguments('gallery_post_ids')}

        for clang in settings.languages:
            doc['name'][clang] = self.get_argument('name.' + clang)
            doc['body'][clang] = self.get_argument('body.' + clang)
            doc['short_desc'][clang] = self.get_argument('short_desc.' + clang)

        if service_id:
            yield models.mongo.services.update({'_id': service_id}, {'$set': doc})
        else:
            doc['_id'] = str(uuid.uuid4())
            yield models.mongo.services.insert(doc)

        self.redirect(self.reverse_url('service.list'))



class BlogCategories(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        action = self.get_argument('action', '')

        if action == 'delete':
            yield models.mongo.blog_categories.remove({'_id': self.get_argument('blog_category_id')})
            self.redirect(self.reverse_url('blog.categories'))
        else:

            category_list = yield models.mongo.blog_categories.find().sort([('order', 1)]).to_list(length=None)

            self.render('admin/pages/manage/blog_categories.html', category_list=category_list)


class BlogCategory(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        blog_category_id = self.get_argument('blog_category_id', '')

        if blog_category_id:
            category = yield models.mongo.blog_categories.find_one({'_id': blog_category_id})
        else:
            category = models.objects.BlogCategory

        self.render('admin/pages/manage/blog_category.html', category=category)

    @tornado.gen.coroutine
    def post(self):

        blog_category_id = self.get_argument('blog_category_id', '')

        doc = {'name': {},
               'slug': self.get_argument('slug'),
               'order': int(self.get_argument('order'))}

        for clang in settings.languages:
            doc['name'][clang] = self.get_argument('name.' + clang)

        if blog_category_id:
            yield models.mongo.blog_categories.update({'_id': blog_category_id}, {'$set': doc})
        else:
            doc['_id'] = str(uuid.uuid4())
            doc['created_at'] = self.now
            doc['created_ip'] = self.remote_ip
            yield models.mongo.blog_categories.insert(doc)

        self.redirect(self.reverse_url('blog.categories'))



class BlogPosts(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        action = self.get_argument('action', '')

        if action == 'delete':
            yield models.mongo.blog_posts.remove({'_id': self.get_argument('blog_id')})
            self.redirect(self.reverse_url('blog.posts'))
        else:

            blog_posts = yield models.mongo.blog_posts.find().sort([('posted_at', pymongo.DESCENDING)]).to_list(length=None)

            blog_categories = yield models.mongo.blog_categories.find().sort([('order', 1)]).to_list(length=None)
            blog_categories_dict = {}
            for blog_category in blog_categories:
                blog_categories_dict[blog_category['_id']] = blog_category

            self.render('admin/pages/manage/blog_posts.html', blog_posts=blog_posts, blog_categories_dict=blog_categories_dict)


class BlogPost(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        blog_id = self.get_argument('blog_id', '')

        if blog_id:
            blog = yield models.mongo.blog_posts.find_one({'_id': blog_id})
        else:
            blog = models.objects.BlogPost
            blog['posted_at'] = self.now

        blog_categories = yield models.mongo.blog_categories.find().sort([('order', 1)]).to_list(length=None)

        self.render('admin/pages/manage/blog_post.html', blog=blog, blog_categories=blog_categories)

    @tornado.gen.coroutine
    def post(self):

        blog_id = self.get_argument('blog_id', '')

        doc = {'slug': self.get_argument('slug'),
               'category_id': self.get_argument('category_id'),
               'posted_at': datetime.datetime.strptime(self.get_argument('posted_at'), '%H:%M %d.%m.%Y'),
               'image_filename': self.get_argument('image_filename', ''),
               'show_on_main_page': bool(int(self.get_argument('show_on_main_page', 0))),
               'name': {},
               'body': {},
               'short_desc': {}}

        for clang in settings.languages:
            doc['name'][clang] = self.get_argument('name.' + clang)
            doc['body'][clang] = self.get_argument('body.' + clang)
            doc['short_desc'][clang] = self.get_argument('short_desc.' + clang)

        if blog_id:
            yield models.mongo.blog_posts.update({'_id': blog_id}, {'$set': doc})
        else:
            doc['_id'] = str(uuid.uuid4())
            doc['created_at'] = self.now
            doc['created_ip'] = self.remote_ip
            yield models.mongo.blog_posts.insert(doc)

        self.redirect(self.reverse_url('blog.posts'))


class GalleryPosts(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        action = self.get_argument('action', '')

        if action == 'delete':
            yield models.mongo.gallery_posts.remove({'_id': self.get_argument('gallery_post_id')})
            self.redirect(self.reverse_url('gallery.posts'))
        else:

            gallery_posts = yield models.mongo.gallery_posts.find().to_list(length=None)

            self.render('admin/pages/manage/gallery_posts.html', gallery_posts=gallery_posts)


class GalleryPost(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        gallery_post_id = self.get_argument('gallery_post_id', '')

        if gallery_post_id:
            gallery_post = yield models.mongo.gallery_posts.find_one({'_id': gallery_post_id})
        else:
            gallery_post = models.objects.GalleryPost

        self.render('admin/pages/manage/gallery_post.html', gallery_post=gallery_post)

    @tornado.gen.coroutine
    def post(self):

        gallery_post_id = self.get_argument('gallery_post_id', '')

        doc = {'name': {},
               'short_desc': {},
               'body': {},
               'slug': self.get_argument('slug'),
               'show_on_main_page': bool(int(self.get_argument('show_on_main_page', 0))),
               'image_filename': self.get_argument('image_filename', ''),
               'order': int(self.get_argument('order'))}

        for clang in settings.languages:
            doc['name'][clang] = self.get_argument('name.' + clang)
            doc['short_desc'][clang] = self.get_argument('short_desc.' + clang)
            doc['body'][clang] = self.get_argument('body.' + clang)

        if gallery_post_id:
            yield models.mongo.gallery_posts.update({'_id': gallery_post_id}, {'$set': doc})
        else:
            doc['_id'] = str(uuid.uuid4())
            doc['created_at'] = self.now
            doc['created_ip'] = self.remote_ip
            yield models.mongo.gallery_posts.insert(doc)

        self.redirect(self.reverse_url('gallery.posts'))



class Messages(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        action = self.get_argument('action', '')

        if action == 'delete':
            yield models.mongo.messages.remove({'_id': self.get_argument('message_id')})
            self.redirect(self.reverse_url('messages'))
        else:
            messages = yield models.mongo.messages.find().sort([('datetime', pymongo.DESCENDING)]).to_list(length=None)

            self.render('admin/pages/manage/messages.html', messages=messages)


class Pages(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):

        pages = yield models.mongo.pages.find().to_list(length=None)

        self.render('admin/pages/manage/pages.html', pages=pages)


class Page(controllers.admin.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):

        page = yield models.mongo.pages.find_one({'_id': self.get_argument('page_id')})

        self.render('admin/pages/manage/page.html', page=page)

    @tornado.gen.coroutine
    def post(self):

        page = yield models.mongo.pages.find_one({'_id': self.get_argument('page_id')})
        if page:
            doc = {'body': {}}
            for clang in settings.languages:
                doc['body'][clang] = self.get_argument('body.' + clang)

            yield models.mongo.pages.update({'_id': page['_id']}, {'$set': doc})

        self.redirect(self.reverse_url('pages'))