# -*- coding: utf-8 -*-
import pymongo
import tornado.web
import tornado.gen
import controllers.base
import uuid
import time
import datetime
import settings
import models
import os
import mimetypes
import tornado.httpclient


class RedirectToLang(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        self.redirect("/%s" % self.lang, True)


class Home(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):

        # content_list = yield models.mongo.content.find().sort([('posted_at', pymongo.DESCENDING)]).to_list(length=None)
        # categories = yield models.mongo.categories.find({}).to_list(length=None)

        main_page_content = yield models.mongo.pages.find_one({'_id': 'main_page'})
        gallery_posts = yield models.mongo.gallery_posts.find({'show_on_main_page': True}).sort([('order', 1)]).limit(30).to_list(length=None)

        self.render('pages/index.html', main_page_content=main_page_content, gallery_posts=gallery_posts)


class ServiceList(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):

        service_list = yield models.mongo.services.find().sort([('order', 1)]).to_list(length=None)

        self.seo_page_title(self._('Services'))
        self.render('pages/service_list.html', service_list=service_list)


class ServiceItem(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self, slug):

        service_item = yield models.mongo.services.find_one({'slug': slug})
        gallery_posts = yield models.mongo.gallery_posts.find({'_id': {'$in': service_item['gallery_post_ids']}}).to_list(length=None)
        if service_item:
            self.seo_page_title('%s - %s' % (self._('Services'), service_item['name'][self.lang]))
            self.render('pages/service_item.html', service_item=service_item, gallery_posts=gallery_posts)
        else:
            raise tornado.web.HTTPError(status_code=404)


class About(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        self.seo_page_title(self._('About'))
        self.render('pages/about.html')


class Contact(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        self.seo_page_title(self._('Contact'))
        self.render('pages/contact.html')


class Blog(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):
        category = self.get_argument('category', '')
        q = {}
        if category:
            category_obj = yield models.mongo.blog_categories.find_one({'slug': category})
            q['category_id'] = category_obj['_id']
            self.seo_page_title('%s - %s' % (self._('Blog'), category_obj['name'][self.lang]))
        else:
            self.seo_page_title(self._('Blog'))

        blog_list = yield models.mongo.blog_posts.find(q).sort([('posted_at', pymongo.DESCENDING)]).to_list(length=None)


        self.render('pages/blog.html', blog_list=blog_list)


class BlogItem(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self, slug):

        blog_item = yield models.mongo.blog_posts.find_one({'slug': slug})
        if blog_item:
            self.seo_page_title('%s - %s' % (self._('Blog'), blog_item['name'][self.lang]))
            self.render('pages/blog_item.html', blog_item=blog_item)
        else:
            raise tornado.web.HTTPError(status_code=404)


class GalleryPosts(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self):

        gallery_posts = yield models.mongo.gallery_posts.find().sort([('order', 1)]).to_list(length=None)

        self.seo_page_title(self._('Gallery'))
        self.render('pages/gallery_posts.html', gallery_posts=gallery_posts)


class GalleryPost(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def get(self, slug):

        gallery_post = yield models.mongo.gallery_posts.find_one({'slug': slug})
        if gallery_post:
            self.seo_page_title('%s - %s' % (self._('Gallery'), gallery_post['name'][self.lang]))
            self.render('pages/gallery_post.html', gallery_post=gallery_post)
        else:
            raise tornado.web.HTTPError(status_code=404)


class Ajax(controllers.base.BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        action = self.get_argument('action')

        res = {'success': False}

        if action == 'send_message':
            yield models.mongo.messages.insert({'_id': str(uuid.uuid4()),
                                                'name': self.get_argument('name', ''),
                                                'datetime': datetime.datetime.now(),
                                                'ip': self.remote_ip,
                                                'contact': self.get_argument('contact', ''),
                                                'message': self.get_argument('message')})
            res['success'] = True

        self.finish(res)