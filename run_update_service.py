# -*- coding: utf-8 -*-
import logging
import settings

import os
import tornado.httpserver
import tornado.ioloop
import tornado.locale
import tornado.web
import controllers.base
import controllers.index
import lib.ui_methods
import lib.ui_modules

logger = logging.getLogger('tornado.general')


handlers = [
    # INDEX-------------------------------------------------------------------------------------------------------------
    tornado.web.url('/get_last_version', controllers.index.GetLastVersion, name='get_last_version'),
    tornado.web.url('/update_last_version', controllers.index.update_last_version, name='update_last_version')
]




application_settings = {
    'template_path': 'templates/',
    'static_path': 'static/caricature',
    'static_url_prefix': '/static/',
    'handlers': handlers,
    'xsrf_cookies': False,
    'debug': settings.debug,
    'login_url': '/ru/profile/login',
    'ui_modules':lib.ui_modules,
    'ui_methods': lib.ui_methods,
    'default_handler_class': controllers.base.NotFound,
    'static_handler_class': controllers.base.BaseHandlerStatic
}


application = tornado.web.Application(**application_settings)


if __name__ == "__main__":

    # If "xheaders" argument is True, tornado uses the "X-Real-Ip" and "X-Forwarded-For" headers
    # for setting self.request.remote_ip property in the request handlers
    http_server = tornado.httpserver.HTTPServer(application, xheaders=True)
    logger.info('Tornado version: %s' % tornado.version)

    if settings.debug:
        # run Tornado in one-process configuration
        http_server.listen(port=settings.ports['caricature_site'], address='0.0.0.0')
    else:
        # run Tornado in multi-processing configuration
        http_server.bind(port=settings.ports['caricature_site'], address='0.0.0.0')
        log_msg = 'Starting Tornado [{role}] with PID: {pid}'
        logger.info(log_msg.format(role='master', pid=os.getpid()))
        # Forks sub-processes (if num_processes == 0, then number of processes
        # will be equal to number of CPU cores)
        http_server.start(num_processes=16)
        logger.info(log_msg.format(role='worker', pid=os.getpid()))

    # configure Tornado loggers
    tornado.log.enable_pretty_logging()
    logging.getLogger('tornado.application').setLevel(logging.ERROR)
    logging.getLogger('tornado.access').setLevel(logging.WARNING)

    print('http://127.0.0.1:' + str(settings.ports['caricature_site']))
    tornado.ioloop.IOLoop.instance().start()
