# -*- coding: utf-8 -*-
import logging
import settings

import os
import tornado.httpserver
import tornado.ioloop
import tornado.log
import tornado.web
import controllers.admin.base
import controllers.admin.index
import lib.ui_methods
import lib.ui_modules

logger = logging.getLogger('tornado.general')


handlers = [
    tornado.web.url('/', controllers.admin.index.Home, name='home'),

    tornado.web.url('/service_list', controllers.admin.index.ServiceList, name='service.list'),
    tornado.web.url('/service_item', controllers.admin.index.ServiceItem, name='service.item'),

    tornado.web.url('/blog_categories', controllers.admin.index.BlogCategories, name='blog.categories'),
    tornado.web.url('/blog_category', controllers.admin.index.BlogCategory, name='blog.category'),

    tornado.web.url('/blog_posts', controllers.admin.index.BlogPosts, name='blog.posts'),
    tornado.web.url('/blog_post', controllers.admin.index.BlogPost, name='blog.post'),

    # tornado.web.url('/gallery_categories', controllers.admin.index.GalleryCategories, name='gallery.categories'),
    # tornado.web.url('/gallery_category', controllers.admin.index.GalleryCategory, name='gallery.category'),

    tornado.web.url('/gallery_posts', controllers.admin.index.GalleryPosts, name='gallery.posts'),
    tornado.web.url('/gallery_post', controllers.admin.index.GalleryPost, name='gallery.post'),

    tornado.web.url('/messages', controllers.admin.index.Messages, name='messages'),

    tornado.web.url('/pages', controllers.admin.index.Pages, name='pages'),
    tornado.web.url('/page', controllers.admin.index.Page, name='page'),
]


application_settings = {
    'template_path': 'templates/',
    'static_path': 'static/ad_admin',
    'static_url_prefix': '/static/',
    'handlers': handlers,
    'xsrf_cookies': False,
    'debug': settings.debug,
    'login_url': '/ru/profile/login',
    'ui_modules':lib.ui_modules,
    'ui_methods': lib.ui_methods
}


application = tornado.web.Application(**application_settings)


if __name__ == "__main__":

    # If "xheaders" argument is True, tornado uses the "X-Real-Ip" and "X-Forwarded-For" headers
    # for setting self.request.remote_ip property in the request handlers
    http_server = tornado.httpserver.HTTPServer(application, xheaders=True)
    logger.info('Tornado version: %s' % tornado.version)

    if settings.debug:
        # run Tornado in one-process configuration
        http_server.listen(port=settings.ports['admin_caricature_site'], address='0.0.0.0')
    else:
        # run Tornado in multi-processing configuration
        http_server.bind(port=settings.ports['admin_caricature_site'], address='0.0.0.0')
        log_msg = 'Starting Tornado [{role}] with PID: {pid}'
        logger.info(log_msg.format(role='master', pid=os.getpid()))
        # Forks sub-processes (if num_processes == 0, then number of processes
        # will be equal to number of CPU cores)
        http_server.start(num_processes=16)
        logger.info(log_msg.format(role='worker', pid=os.getpid()))

    # configure Tornado loggers
    tornado.log.enable_pretty_logging()
    logging.getLogger('tornado.application').setLevel(logging.ERROR)
    logging.getLogger('tornado.access').setLevel(logging.WARNING)

    print('http://127.0.0.1:' + str(settings.ports['admin_caricature_site']))
    tornado.ioloop.IOLoop.instance().start()
