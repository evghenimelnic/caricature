import motor
import funcy
import tornado.httpclient
import tornado.gen
import settings
import redis


class AsyncHTTPClient(object):
    @funcy.cached_property
    def client(self):
        tornado.httpclient.AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient", max_clients=200)
        return tornado.httpclient.AsyncHTTPClient()


class Mongo(object):

    @funcy.cached_property
    def services(self):
        return motor.MotorClient(settings.mongodb).caricature.services

    @funcy.cached_property
    def blog_categories(self):
        return motor.MotorClient(settings.mongodb).caricature.blog_categories

    @funcy.cached_property
    def blog_posts(self):
        return motor.MotorClient(settings.mongodb).caricature.blog_posts

    # @funcy.cached_property
    # def gallery_categories(self):
    #     return motor.MotorClient(settings.mongodb).caricature.gallery_categories

    @funcy.cached_property
    def gallery_posts(self):
        return motor.MotorClient(settings.mongodb).caricature.gallery_posts

    @funcy.cached_property
    def messages(self):
        return motor.MotorClient(settings.mongodb).caricature.messages

    @funcy.cached_property
    def pages(self):
        return motor.MotorClient(settings.mongodb).caricature.pages

    @funcy.cached_property
    def auto_increments(self):
        return motor.MotorClient(settings.mongodb).caricature.auto_increments

    @tornado.gen.coroutine
    def next_increment(self, key):
        new_id = yield self.auto_increments.find_and_modify({'_id': key}, {'$inc': {'seq': 1}}, new=True, upsert=True)
        raise tornado.gen.Return(new_id['seq'])


async_http = AsyncHTTPClient()
mongo = Mongo()
