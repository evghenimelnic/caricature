

ServiceItem = {'_id': '',
          'name': {'en': '', 'fr': ''},
          'slug': '',
          'order': 0,
          'show_on_main_page': False,
          'image_filename': '',
          'short_desc': {'en': '', 'fr': ''},
          'body': {'en': '', 'fr': ''},
          'gallery_post_ids': []}

BlogPost = {'_id': '',
          'name': {'en': '', 'fr': ''},
          'short_desc': {'en': '', 'fr': ''},
          'category_id': '',
          'image_filename': '',
'show_on_main_page': False,
          'slug': '',
          'order': 0,
          'body': {'en': '', 'fr': ''},
          'meta_keywords': {'en': '', 'fr': ''},
          'meta_description': {'en': '', 'fr': ''}}

BlogCategory = {'_id': '',
                'name': {'en': '', 'fr': ''},
                'slug': '',
                'order': 0}


GalleryPost = {'_id': '',
          'name': {'en': '', 'fr': ''},
          'short_desc': {'en': '', 'fr': ''},
'show_on_main_page': False,
          'slug': '',
          'image_filename': '',
          'order': 0,
          'body': {'en': '', 'fr': ''}}
